/*
 *	File: <netdb.h> For WINDOWS MFC
 *
 *	Created on: Dec 21, 2015
 *	Author    : Davit Kalantaryan (Email: davit.kalantaryan@desy.de)
 *
 *
 */
#ifndef __win_grp_h__
#define __win_grp_h__

#include <first_includes/common_include_for_headers.h>
#include <sys/cdefs.h>
#include <redesigned/sys/types.h>
#include <unix_like_user_group_header.h>


__BEGIN_C_DECLS


__END_C_DECLS


#endif  // #ifndef __win_grp_h__
