
// process.h
// 2017 Dec 07
// created by D. Kalantaryan

#ifndef __wlac_redesigend_process_h__
#define __wlac_redesigend_process_h__

#pragma include_alias( <process.h>, <process.h> )
#include <process.h>
#pragma include_alias( <process.h>, <redesigned/process.h> )

#define getpid _getpid

#endif  // #ifndef __wlac_redesigend_process_h__
